<?php
/**
 * @file
 * Contains \Drupal\article\Plugin\Block\XaiBlock.
 */

namespace Drupal\visual\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * Provides a 'visual' block.
 *
 * @Block(
 *   id = "visual_block",
 *   admin_label = @Translation("Visual block"),
 *   category = @Translation("Custom Visual block example")
 * )
 */
class VisualBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return array(
      '#type' => 'markup',
      '#markup' => 'A class is a template for objects, and an object is an instance of class.',
    );
  }
}
